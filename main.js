var firebase = new Firebase('https://groomsmen.firebaseio.com');
filepicker.setKey('AXgXOWoJS5q5Kyfh37FrWz');

function getURLParameter(name) {
    return decodeURI(
        (RegExp(name + '=' + '(.+?)(&|$)').exec(location.search)||[,null])[1]
    );
}

$(function(){

    var user = {};
    firebase.child('groomsmen').child(getURLParameter('id')).once('value',function(user_data){
        if(user_data.val() != null){
           
            user = user_data.val();
            user.uid = user_data.name();
            user.ref = user_data.ref();

            $('.full_name').text("Mr. "+user.first_name+' '+user.last_name);

            
            user.ref.child('accepted').on('value',function(data){
                data.val() ? accept() : unaccept();
            });
        }
    });


    function accept(){
        $('#unaccepted').fadeOut(function(){
            $('body').addClass('evil');
            $('#accepted').fadeIn().removeClass('hidden');
        });

       

        firebase.child('groomsmen').once('value', function(men){
            $('#groomsmen_list').empty();
            men.forEach(function(man){
                $('#groomsmen_list').append('<li id='+man.name()+'><i class="icon-li icon-check-empty"></i>'+man.val().first_name+' '+man.val().last_name+'</li>');
            
                man.ref().child('accepted').on('value', function(accepted){
                    var li_ref = $('#'+man.name()+' i');
                    if(accepted.val()){
                        li_ref.removeClass('icon-check-empty');
                        li_ref.addClass('icon-check');
                    }
                    else{
                        li_ref.removeClass('icon-check');
                        li_ref.addClass('icon-check-empty');
                    }
                });
            });
        });

        firebase.child('comments').on('child_added', function(comment){
            var html_string = "<div class='comment'><p class='name'>"+comment.val().name+":</p>";
            if(comment.val().img){
                html_string += "<a href="+comment.val().img+"><img class='comment_img' src="+comment.val().img+" /></a>"
            }
            html_string += "<p class='text'>"+comment.val().text+"</p></div>"

            $('.comments').append(
                html_string
            );
            var objDiv = document.getElementById("comments");
            objDiv.scrollTop = objDiv.scrollHeight;
        });
    }

    function unaccept(){
        $('#accepted').fadeOut(function() {
            $('body').removeClass('evil');
            $('#unaccepted').fadeIn().removeClass('hidden');    
        });
        $('#accept-btn').click(function(){user.ref.child('accepted').set(true)});
    }

    $('.comment_btn').click(function (){
        var s_text = $('.comment_form').val();
        var url = $('.preview-img').data('url')
        var comment = {}
        if(s_text || url){
            comment.name = user.first_name+' '+user.last_name;
            comment.text = s_text;
            if(url){
                comment.img= url;
            }
            firebase.child('comments').push(comment);
            $('.comment_form').val('');
            $('.preview-img').remove();
        }
    });

    $('.upload_btn').click(function(){
        filepicker.pick({
            mimetypes: ['image/*', 'text/plain'],
            container: 'window',
          
          },
          function(stuff){
            $('#chat-btns').append("<img class='preview-img' src="+stuff.url+" >");
            $('.preview-img').data('url', stuff.url);
            console.log(JSON.stringify(stuff));
          },
          function(FPError){
            console.log(FPError.toString());
          }
        );
    });
});


function parallax(){
    var scrolled = $(window).scrollTop();
    $('.bg').css('top', -(scrolled * 0.3)+'px');
}

$(window).scroll(function(e){
    parallax();
});

